pub mod flat;
pub mod map;

#[cfg(doctest)]
mod readme_doctests {
    macro_rules! external_doc_test {
        ($x:expr) => {
            #[doc = $x]
            struct ReadmeDoctests;
        };
    }

    external_doc_test!(include_str!("../README.md"));
}
